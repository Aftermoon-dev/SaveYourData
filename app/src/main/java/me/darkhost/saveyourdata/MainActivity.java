package me.darkhost.saveyourdata;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import me.darkhost.saveyourdata.customview.appListAdapter;
import me.darkhost.saveyourdata.db.DBManager;
import me.darkhost.saveyourdata.service.ServiceControllerService;
import me.darkhost.saveyourdata.util.Util;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    boolean isEnabled = true;
    boolean isFirstLaunch = true;
    long backPressedTime;
    ListView mListView;
    appListAdapter Adapter;
    DBManager dbManager;
    Toolbar toolbar;
    ShowcaseView showcaseView;
    int showcaseCounter;
    AdView main_adView;
    SharedPreferences sharedPreferences;
    int Request_Lollipop_UsageStats = 10;
    boolean isDisabledAdinData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 마시멜로우 이상 버전에서의 퍼미션 권한 허용을 위한 작업
        getPermission();

        // 롤리팝 버전에서의 현재 실행중인 Foreground 앱을 받아오기 위한 권한 허용 작업
        getLolipopPermission();

        // Toolbar 버튼 적용
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        // 저장값 로드
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        LoadSP();

        // SQLite DB 로드
        dbManager = new DBManager(MainActivity.this, "applist.db", null, 1);

        // 리스트뷰 설정
        setListview();

        Adapter.notifyDataSetChanged();
    }

    // 메뉴
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setting_btn:
                Intent i = new Intent(MainActivity.this, SettingActivity.class);
                startActivityForResult(i, 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Back Button Event
    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && 2000 >= intervalTime) {
            super.onBackPressed();
        }
        else {
            backPressedTime = tempTime;
            Toast.makeText(this, getString(R.string.backbutton), Toast.LENGTH_SHORT).show();
        }
    }

    public void onDestroy() {
        main_adView.destroy();
        super.onDestroy();
    }

    public void onResume() {
        // Admob Setting
        setAdmob();

        super.onResume();
    }

    // 마시멜로우 이상 버전에서의 퍼미션 권한 허용을 위한 작업
    public void getPermission()
    {
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            PermissionListener permissionlistener = new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    Log.d("MainActivity", "Permission Granted!");
                    // 권한 없는데 서비스 시작하면 null이 리턴되므로 서비스 세팅은 권한이 허용되었을때만 한다
                    setService();
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                    Log.d("MainActivity", "Permission Denied!");
                    Toast.makeText(MainActivity.this, getString(R.string.permissiondwarning), Toast.LENGTH_LONG).show();
                    finish();
                }
            };

            new TedPermission(this)
                    .setPermissionListener(permissionlistener)
                    .setRationaleMessage(getString(R.string.permissionwarning))
                    .setDeniedMessage(getString(R.string.permissiondwarning))
                    .setPermissions(Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.PACKAGE_USAGE_STATS)
                    .check();
        }
    }

    // 읽기
    public void LoadSP() {
        isEnabled = sharedPreferences.getBoolean("enabledSwc", true);
        if(isEnabled) Log.d("LoadSP", "isEnabled true");

        // 첫 실행인지 확인하기 위해 가져옴 (기본값은 true)
        SharedPreferences firstLaunch = getSharedPreferences("isFirstLaunch", MODE_PRIVATE);
        isFirstLaunch = firstLaunch.getBoolean("isFirstLaunch", true);
    }

    // 서비스 세팅
    public void setService() {
        // 서비스 컨트롤러 세팅
        // 해당 서비스가 모든 서비스의 온오프를 관리함
        // 이 서비스를 끄면 모든 서비스 및 리시버가 종료됨

        // 서비스가 돌아가고 있지 않으면서 활성화 상태에 체크되있는 경우
        if(!Util.isMyServiceRunning(getApplicationContext(), ServiceControllerService.class) && isEnabled) {
            // 서비스 시작
            Intent i = new Intent("me.darkhost.saveyourdata.servicecontrollerservice");
            i.setPackage("me.darkhost.saveyourdata");
            startService(i);
        }

        // 서비스가 돌아가고 있으면서 비활성화 상태에 체크되있는 경우
        if(Util.isMyServiceRunning(getApplicationContext(), ServiceControllerService.class) && !isEnabled) {
            // 서비스 멈춤
            Intent i = new Intent("me.darkhost.saveyourdata.servicecontrollerservice");
            i.setPackage("me.darkhost.saveyourdata");
            stopService(i);
        }
    }

    static class setListViewProcess extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;
        private WeakReference<MainActivity> activityWeakReference;

        setListViewProcess(MainActivity context) {
            activityWeakReference = new WeakReference<>(context);
        }

        protected void onPreExecute() {
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing()) return;
            dialog = new ProgressDialog(activity);
            super.onPreExecute();
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage(activity.getString(R.string.AppList_ProgressText));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing()) return null;
            // 시스템 앱 포함 여부 가져오기
            boolean isSystemAppInclude = activity.sharedPreferences.getBoolean("includeSystemApp", false);

            // 설치된 앱 정보 가져오기
            PackageManager packageManager = activity.getPackageManager();
            List<ApplicationInfo> ApplicationList = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
            for (ApplicationInfo applicationInfo : ApplicationList) {

                // 시스템 앱 포함을 원할경우
                if(isSystemAppInclude) {
                    // 앱 패키지명 구하기
                    String AppPackage = applicationInfo.packageName;

                    // 앱 이름 구하기
                    String AppLabel = packageManager.getApplicationLabel(applicationInfo).toString();

                    // 앱 아이콘 구하기
                    Drawable d = null;
                    try {
                        Bitmap bitmap;
                        if (Build.VERSION.SDK_INT >= 26) {
                            bitmap = Util.getApplicationIconFromPackageName_O(packageManager, AppPackage);
                        }
                        else {
                            Drawable AppIcon = activity.getPackageManager().getApplicationIcon(applicationInfo);
                            bitmap = ((BitmapDrawable) AppIcon).getBitmap();
                        }
                        d = new BitmapDrawable(activity.getResources(), Bitmap.createScaledBitmap
                                (bitmap, ((int) Util.convertDpToPixel(50, activity)),  ((int) Util.convertDpToPixel(50, activity)), true));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                    // 경고가 연속으로 뜨는걸 방지하기 위해 SaveYourData는 제외
                    if(!AppPackage.equals(BuildConfig.APPLICATION_ID)) {
                        // 어댑터에 아이템 추가
                        activity.Adapter.addItem(AppPackage, AppLabel, d);
                    }
                }
                else {
                    if(!Util.isSystemPackage(applicationInfo)) {
                        // 앱 패키지명 구하기
                        String AppPackage = applicationInfo.packageName;

                        // 앱 이름 구하기
                        String AppLabel = packageManager.getApplicationLabel(applicationInfo).toString();

                        // 앱 아이콘 구하기
                        Drawable d = null;
                        try {
                            Bitmap bitmap;
                            if (Build.VERSION.SDK_INT >= 26) {
                                bitmap = Util.getApplicationIconFromPackageName_O(packageManager, AppPackage);
                            }
                            else {
                                Drawable AppIcon = activity.getPackageManager().getApplicationIcon(applicationInfo);
                                bitmap = ((BitmapDrawable) AppIcon).getBitmap();
                            }
                            d = new BitmapDrawable(activity.getResources(), Bitmap.createScaledBitmap
                                    (bitmap, ((int) Util.convertDpToPixel(50, activity)),  ((int) Util.convertDpToPixel(50, activity)), true));
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                        // 경고가 연속으로 뜨는걸 방지하기 위해 SaveYourData는 제외
                        if(!AppPackage.equals(BuildConfig.APPLICATION_ID)) {
                            // 어댑터에 아이템 추가
                            activity.Adapter.addItem(AppPackage, AppLabel, d);
                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String strings) {
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing()) return;
            super.onPostExecute(strings);
            dialog.dismiss();
            // 순서대로 정렬
            activity.Adapter.sort();
            activity.Adapter.notifyDataSetChanged();
        }
    }

    // 리스트뷰 설정
    public void setListview() {
        Adapter = new appListAdapter(this);
        mListView = findViewById(R.id.applist);
        mListView.setAdapter(Adapter);
        mListView.setOnItemClickListener(this);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        new setListViewProcess(MainActivity.this).execute("");
        Adapter.notifyDataSetChanged();
    }

    // 리스트뷰 아이템 클릭 이벤트
    @Override
    public void onItemClick(AdapterView arg0, View v, int position, long arg3) {
        CheckBox checkBox = v.findViewById(R.id.checkBox);
        checkBox.performClick();

        // 체크되어 있을경우
        if(checkBox.isChecked()) {
            Log.d("MainActivity", checkBox.getTag().toString() + " REG");
            dbManager.AppDataInsert(checkBox.getTag().toString(), checkBox.isChecked());
        }
        // 체크가 되어있지 않을 경우
        else {
            Log.d("MainActivity", checkBox.getTag().toString() + " UNREG");
            dbManager.AppDataDelete(checkBox.getTag().toString());
        }
        Adapter.notifyDataSetChanged();
    }

    // 롤리팝 버전에서의 현재 실행중인 Foreground 앱을 받아오기 위한 권한 허용 작업
    public void getLolipopPermission() {
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.d("MainActivity", "SDK_INT => Lolipop");
            if(android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP || android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
                Log.d("MainActivity", "Lolipop == SDK_INT");
                if(!Util.hasUsageStatsPermission(getApplicationContext())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(getString(R.string.warning))
                            .setMessage(getString(R.string.lolipop_permissionwarning))
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(MainActivity.this, getString(R.string.lolipop_pwarning1), Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                                    startActivityForResult(intent, Request_Lollipop_UsageStats);
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                // 권한이 허용됬을경우
                else {
                    Log.d("MainActivity", "Permission Granted!");
                    // 권한 없는데 서비스 시작하면 null이 리턴되므로 서비스 세팅은 권한이 허용되었을때만 한다
                    setService();
                }
            }
        }
    }

    // 쇼케이스 뷰 설정
    public void setShowcaseView() {
        // 쇼케이스뷰 카운터 초기화
        showcaseCounter = 0;

        // 다음 실행때는 쇼케이스뷰가 뜨지 않도록 조치함
        isFirstLaunch = false;
        SharedPreferences firstLaunch = getSharedPreferences("isFirstLaunch", MODE_PRIVATE);
        SharedPreferences.Editor firstLaunchEditor = firstLaunch.edit();
        firstLaunchEditor.putBoolean("isFirstLaunch", isFirstLaunch);
        firstLaunchEditor.apply();

        // 설정 버튼 쇼케이스뷰 적용
        final Target settingtarget = new Target() {
            @Override
            public Point getPoint() {
                return new ViewTarget(toolbar.findViewById(R.id.setting_btn)).getPoint();
            }
        };

        final Target checkboxtarget = new Target() {
            @Override
            public Point getPoint() {
                return new ViewTarget(mListView.getChildAt(0).findViewById(R.id.checkBox)).getPoint();
            }
        };

        showcaseView = new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setContentTitle(getString(R.string.welcome_showcasetitle))
                .setContentText(getString(R.string.welcome_showcasetext))
                .setStyle(R.style.ShowcaseView_SaveYourData)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                       switch (showcaseCounter) {
                           case 0:
                               showcaseView.setShowcase(checkboxtarget, true);
                               showcaseView.setContentTitle(getString(R.string.checkbox_showcasetitle));
                               showcaseView.setContentText(getString(R.string.checkbox_showcasetext));
                               showcaseView.setButtonText(getString(R.string.next));
                               break;
                           case 1:
                               showcaseView.setShowcase(settingtarget, true);
                               showcaseView.setContentTitle(getString(R.string.setting));
                               showcaseView.setContentText(getString(R.string.setting_showcasetext));
                               showcaseView.setButtonText(getString(R.string.next));
                               break;
                           case 2:
                               showcaseView.setShowcase(Target.NONE, true);
                               showcaseView.setContentTitle(getString(R.string.tip_showcasetitle));
                               showcaseView.setContentText(getString(R.string.tip1_showcasetext));
                               showcaseView.setButtonText(getString(R.string.next));
                               break;
                           case 3:
                               showcaseView.setShowcase(Target.NONE, true);
                               showcaseView.setContentTitle(getString(R.string.thankyou_showcasetitle));
                               showcaseView.setContentText(getString(R.string.thankyou_showcasetext));
                               showcaseView.setButtonText(getString(R.string.ok));
                               break;
                           case 4:
                               showcaseView.hide();
                               break;
                       }
                        showcaseCounter++;
                    }
                })
                .build();
    }

    // 광고 설정
    public void setAdmob() {
        isDisabledAdinData = sharedPreferences.getBoolean("disableAdinData", false);
        // 네트워크가 연결되있을때만 광고가 뜨도록 설정
        ConnectivityManager CManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo NetworkInfo = CManager.getActiveNetworkInfo();
        main_adView = findViewById(R.id.adView);
        if (NetworkInfo != null) {
            MobileAds.initialize(getApplicationContext(), getString(R.string.main_admob_id));
            // 설정에서 데이터 네트워크에서의 광고를 껐을 경우 안뜨게 하도록 함
            if(NetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE && isDisabledAdinData) {
                main_adView.setEnabled(false);
                main_adView.setVisibility(View.GONE);
            }
            else {
                AdRequest main_adRequest = new AdRequest.Builder()
                        .addTestDevice("B33C0AA21FDA78A6ED7A91C5807BAB58")
                        .build();
                main_adView.setVisibility(View.VISIBLE);
                main_adView.setEnabled(true);
                main_adView.loadAd(main_adRequest);
            }
        }
        else {
            main_adView.setEnabled(false);
            main_adView.setVisibility(View.GONE);
        }
    }

    // Activity Result
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1) {
            if (resultCode == RESULT_OK) {
                if (data.getBooleanExtra("TutorialStart", false)) {
                    setShowcaseView();
                }

                mListView.removeAllViewsInLayout();
                Adapter.notifyDataSetChanged();
                setListview();
            }
        }

        if(requestCode == Request_Lollipop_UsageStats) {
            if(Util.hasUsageStatsPermission(getApplicationContext())) {
                Log.d("MainActivity", "Permission Granted!");
                   // 권한 없는데 서비스 시작하면 null이 리턴되므로 서비스 세팅은 권한이 허용되었을때만 한다
                setService();
            }
            else {
                Log.d("MainActivity", "Permission Denied!");
                getLolipopPermission();
            }
        }
    }
}