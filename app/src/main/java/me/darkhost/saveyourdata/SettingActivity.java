package me.darkhost.saveyourdata;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.preference.SwitchPreference;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import me.darkhost.saveyourdata.service.ServiceControllerService;
import me.darkhost.saveyourdata.util.Util;

/**
 * Created by 민재 on 2017-01-01.
 */

public class SettingActivity extends AppCompatActivity {
    static boolean isCheckedIncludeSystemApp = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(getString(R.string.setting));
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PreferenceFragments()).commit();
    }

    public static class PreferenceFragments extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.pref_setting);
            setOnPreferenceChange(findPreference("warning_method"));
            setOnPreferenceChange(findPreference("warning_mode"));
            setOnPreferenceChange(findPreference("NotiSound"));

            Preference version = findPreference("version");
            version.setSummary(BuildConfig.VERSION_NAME);

            Preference returorial = findPreference("tutorialrestart");
            returorial.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent();
                    intent.putExtra("TutorialStart", true);
                    getActivity().setResult(RESULT_OK, intent);
                    getActivity().finish();
                    return true;
                }
            });

            SwitchPreference enabledSwitchP = (SwitchPreference) findPreference("enabledSwc");
            enabledSwitchP.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    boolean isChecked = (Boolean) o;

                    if(isChecked) {
                        if(!Util.isMyServiceRunning(getActivity(), ServiceControllerService.class)) {
                            Intent i = new Intent("me.darkhost.saveyourdata.servicecontrollerservice");
                            i.setPackage("me.darkhost.saveyourdata");
                            getActivity().startService(i);
                        }
                    }
                    else {
                        if(Util.isMyServiceRunning(getActivity(), ServiceControllerService.class)) {
                            Intent i = new Intent("me.darkhost.saveyourdata.servicecontrollerservice");
                            i.setPackage("me.darkhost.saveyourdata");
                            getActivity().stopService(i);
                        }
                    }

                    // 알림 삭제
                    Util.cancelNotification(getActivity(), 1);

                    // No 선택 여부 초기화
                    SharedPreferences selectNo = getActivity().getSharedPreferences("SelectNo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = selectNo.edit();
                    editor.putBoolean("SelectNo", false);
                    editor.apply();

                    return true;
                }
            });
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

        }

        // http://itmir.tistory.com/523
        private void setOnPreferenceChange(Preference mPreference) {
            mPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);
            onPreferenceChangeListener.onPreferenceChange(mPreference, PreferenceManager.getDefaultSharedPreferences(mPreference.getContext()).getString(mPreference.getKey(), ""));
        }

        private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String stringValue = newValue.toString();
                if (preference instanceof ListPreference) {
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);
                    preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);

                    if(preference.getKey().equals("warning_method")) {
                        ListPreference setmode = (ListPreference) findPreference("warning_mode");
                        RingtonePreference notisound = (RingtonePreference) findPreference("NotiSound");
                        SwitchPreference newapp = (SwitchPreference) findPreference("newAppNoti");
                        newapp.setEnabled(false);
                        if(index == 1) {
                            setmode.setEnabled(true);
                            notisound.setEnabled(true);
                        }
                        else {
                            setmode.setEnabled(false);
                            notisound.setEnabled(false);
                        }
                    }

                } else if (preference instanceof RingtonePreference) {
                    if (TextUtils.isEmpty(stringValue)) {
                        // Empty values correspond to 'silent' (no ringtone).
                        preference.setSummary(getString(R.string.setting_noti_slient));

                    } else {
                        Ringtone ringtone = RingtoneManager.getRingtone(preference.getContext(), Uri.parse(stringValue));

                        if (ringtone == null) {
                            // Clear the summary if there was a lookup error.
                            preference.setSummary(null);

                        } else {
                            String name = ringtone.getTitle(preference.getContext());
                            preference.setSummary(name);
                        }
                    }
                }
                return true;
            }
        };

        public void onDestroy() {
            Log.d("Setting", "DESTROY");
            super.onDestroy();
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}
