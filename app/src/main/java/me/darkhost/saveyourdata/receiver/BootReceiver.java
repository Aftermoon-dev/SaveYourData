package me.darkhost.saveyourdata.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import me.darkhost.saveyourdata.service.ServiceControllerService;
import me.darkhost.saveyourdata.util.Util;

/**
 * Created by 민재 on 2017-01-10.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            if(!Util.isMyServiceRunning(context, ServiceControllerService.class)) {
                // 서비스 시작
                Log.d("BootReceiver", "ServiceControllerService Start!");
                Intent i = new Intent("me.darkhost.saveyourdata.servicecontrollerservice");
                i.setPackage("me.darkhost.saveyourdata");
                if (Build.VERSION.SDK_INT >= 26) {
                    context.startForegroundService(i);
                }
                else context.startService(i);
            }
        }
    }
}
