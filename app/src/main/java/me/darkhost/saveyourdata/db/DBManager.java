package me.darkhost.saveyourdata.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by 민재 on 2016-12-31.
 */

public class DBManager extends SQLiteOpenHelper {

    public DBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // 최초 실행시 테이블이 없으면 테이블을 생성함
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE applist(\" _id\" INTEGER PRIMARY KEY AUTOINCREMENT, \"packagename\" VARCHAR(256), \"check\" INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d("DBManager", "Application Upgrade");
    }

    // 데이터 삽입 함수
    public void AppDataInsert(String AppPackage, boolean isChecked) {
        int Checked;
        if(isChecked) Checked = 1; else Checked = 0;

        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO applist (\"packagename\", \"check\") values('" + AppPackage + "', " + Checked + ");");
        db.close();
    }

    // 데이터 제거 함수
    public void AppDataDelete(String AppPackage) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE from applist where \"packagename\" = '" + AppPackage + "';");
        db.close();
    }

    // 전체 앱 목록 가져오는 함수
    public ArrayList<String> getAllTargetData() {
        ArrayList<String> appList = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("select \"packagename\" from applist where \"check\" = 1", null);
        while(cursor.moveToNext()) {
            appList.add(cursor.getString(0));
        }
        cursor.close();
        db.close();
        return appList;
    }

    // 특정 패키지가 DB 내 존재 여부 확인
    public Boolean getExistPackage(String AppPackage) {
        boolean isExist = false;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("select \"packagename\" from applist where \"packagename\" = \"" + AppPackage + "\"", null);
        isExist = cursor.moveToFirst();
        cursor.close();
        db.close();
        String value = String.valueOf(isExist);
        Log.d("DBManager", AppPackage + " is Listin ->" + value);
        return isExist;
    }

    // DB 데이터 초기화
    public void AppDataReset() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE from applist");
        db.close();
    }
}
