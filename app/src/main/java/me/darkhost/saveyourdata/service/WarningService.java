package me.darkhost.saveyourdata.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.rvalerio.fgchecker.AppChecker;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import me.darkhost.saveyourdata.DialogActivity;
import me.darkhost.saveyourdata.R;
import me.darkhost.saveyourdata.db.DBManager;
import me.darkhost.saveyourdata.util.Util;

import static me.darkhost.saveyourdata.util.Util.getApplicationIconFromPackageName_O;

/**
 * Created by 민재 on 2016-12-11.
 */

public class WarningService extends Service {
    // 스케쥴러
    ScheduledExecutorService scheduler;

    // 활성화 여부 함수
    boolean isEnabled = true;

    // SP
    SharedPreferences sharedPreferences;

    // DB
    DBManager dbManager;

    // 알림 형식 체크
    boolean isNoti = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        // Android O 이상에서의 Error 방지를 위해 startForeground를 실행해주어야 함.
        startForeground(1000, new Notification());

        // DBManager 등록
        dbManager = new DBManager(getApplicationContext(), "applist.db", null, 1);

        // 서비스 시작시 이전 앱 기록 삭제
        SharedPreferences beforeApps = getSharedPreferences("BeforeApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor beforeAppeditor = beforeApps.edit();
        beforeAppeditor.putString("BeforeApp", "0");
        beforeAppeditor.apply();

        // 아니오 선택 여부 초기화
        SharedPreferences selectNo = getSharedPreferences("SelectNo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = selectNo.edit();
        editor.putBoolean("SelectNo", false);
        editor.apply();
    }

    // 서비스 멈출경우
    public void onDestroy() {
        // Scheduler를 종료함
        if(scheduler != null) {
            scheduler.shutdown();
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("WarningService", "Service Start!");

        // 저장 정보 가져오기
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        isEnabled = sharedPreferences.getBoolean("enabledSwc", true);

        // 활성화가 되어있을 경우
        if(isEnabled) {
            // 앱을 구동할때마다 이벤트가 있지는 않아서 어쩔 수 없이 Foreground를 2초마다 체크함
            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    Log.d("WarningService", "TimerTask Start!");
                    // 현재 연결된 네트워크가 모바일 네트워크 (3G / LTE)인지 확인
                    ConnectivityManager CManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                    NetworkInfo NetworkInfo = CManager.getActiveNetworkInfo();

                    // 알림 형식 체크
                    int index = Util.getArrayIndex(getApplicationContext(), R.array.setting_warningmethod_value, sharedPreferences.getString("warning_method", "0"));
                    if(index == 0 ) {
                        Util.cancelNotification(getApplicationContext(), Notification.PRIORITY_HIGH);
                        isNoti = false;
                    }
                    else if(index == 1) {
                        isNoti = true;
                    }

                    // 이유는 모르겠지만 Wi-Fi와 모바일 네트워크가 둘 다 켜져있지 않을경우 null이 되는지 코드를 실행하면 튕기는 현상이 있어서 null 체크를 함
                    if (NetworkInfo != null) {
                        // 모바일 네트워크로 사용중인 경우
                        if (NetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                            Log.d("WarningService", "Mobile Data On");

                            // Get App List
                            ArrayList<String> applist = dbManager.getAllTargetData();
                            for (int i = 0; i < applist.size(); i++) {
                                Log.d("WarningService", "for " + Integer.toString(i));

                                // 알림을 띄울 앱인지 확인을 위해 현재 작동중인 앱이 목록에 있는 앱인지 체크
                                boolean isForegroundApp = isForegroundApp(getApplicationContext(), applist.get(i));

                                // 목록에 있는 앱이 맞을 경우
                                if (isForegroundApp) {
                                    Log.d("WarningService", "for " + Integer.toString(i) + " is in AppList! Warning Start!");

                                    // Dialog Intent 설정
                                    Intent dialog = new Intent(getApplicationContext(), DialogActivity.class);
                                    dialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    dialog.putExtra("DialogType", 0);
                                    dialog.putExtra("AppPackage", applist.get(i));

                                    // Notification 알림으로 받기를 설정했을 경우
                                    if(isNoti) {
                                        Log.d("WarningService", "isNoti = true");
                                        // 이전 실행 앱 패키지 이름 체크
                                        SharedPreferences getBeforeApp = getSharedPreferences("BeforeApp", MODE_PRIVATE);
                                        String BeforeApp = getBeforeApp.getString("BeforeApp", "");

                                        // 이전 앱과 패키지명이 다른 경우
                                        if(!BeforeApp.equals(applist.get(i))) {
                                            Log.d("WarningService", "Before Not Same Package");
                                            // icon bitmap
                                            Bitmap bitmaps;
                                            if (Build.VERSION.SDK_INT >= 26) {
                                                bitmaps = getApplicationIconFromPackageName_O(getPackageManager(), applist.get(i));
                                            }
                                            else {
                                                BitmapDrawable drawable = (BitmapDrawable) Util.getApplicationIconFromPackageName(getApplicationContext(), applist.get(i));
                                                bitmaps = drawable.getBitmap();
                                            }

                                            // 경고 문구 설정
                                            String WarningText = String.format(getString(R.string.notiwarning), Util.getApplicationLabelFromPackageName(getApplicationContext(), applist.get(i)));
                                            // Pending Intent 설정
                                            PendingIntent NotiIntent = PendingIntent.getActivity(getApplicationContext(), 0, dialog, PendingIntent.FLAG_UPDATE_CURRENT);
                                            // Notification 설정
                                            NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                                            // Notification Oreo 대응 - NotificationChannel
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                NotificationChannel channel = new NotificationChannel("warning_channel", getString(R.string.Warning_NotiChannel), NotificationManager.IMPORTANCE_HIGH);
                                                channel.setDescription("Application Warning in Mobile Data");
                                                channel.enableLights(true);
                                                channel.setLightColor(Color.CYAN);
                                                manager.createNotificationChannel(channel);
                                            }

                                            NotificationCompat.Builder WarningNoti = new NotificationCompat.Builder(getApplicationContext(), "warning_channel")
                                                    .setContentTitle(getString(R.string.app_name))
                                                    .setContentText(WarningText)
                                                    .setTicker(WarningText)
                                                    .setSmallIcon(R.drawable.ic_warning_white_24dp)
                                                    .setLargeIcon(bitmaps)
                                                    .setAutoCancel(true)
                                                    .setContentIntent(NotiIntent)
                                                    .setPriority(Notification.PRIORITY_HIGH);
                                                String Sound = sharedPreferences.getString("NotiSound", "content://settings/system/notification_sound");
                                                int warningmode_int = Util.getArrayIndex(getApplicationContext(), R.array.setting_warningmode_value, sharedPreferences.getString("warning_mode", "0"));
                                                if (warningmode_int == 0) {
                                                    WarningNoti.setSound(Uri.parse(Sound));
                                                    WarningNoti.setDefaults(Notification.DEFAULT_VIBRATE);
                                                } else if (warningmode_int == 1) {
                                                    WarningNoti.setSound(Uri.parse(Sound));
                                                } else if (warningmode_int == 2) {
                                                    WarningNoti.setDefaults(Notification.DEFAULT_VIBRATE);
                                                }
                                                manager.notify(1, WarningNoti.build());
                                            }
                                        else {
                                            Log.d("WarningService", "Before Same Package");
                                        }
                                    }
                                    // 일반 알림으로 받기를 설정했을 경우
                                    else
                                    {
                                        Log.d("WarningService", "isNoti = false");
                                        startActivity(dialog);
                                    }
                                }
                                else {
                                    Log.d("WarningService", "for " + Integer.toString(i) + " is Not Target");
                                }
                                Log.d("WarningService", "for " + Integer.toString(i) + " is " + applist.get(i));
                            }
                            // 현재 앱을 다음에 체크할 수 있도록 저장함
                            SharedPreferences beforeApps = getSharedPreferences("BeforeApp", Context.MODE_PRIVATE);
                            SharedPreferences.Editor beforeAppeditor = beforeApps.edit();
                            AppChecker appChecker = new AppChecker();
                            beforeAppeditor.putString("BeforeApp", appChecker.getForegroundApp(getApplicationContext()));
                            beforeAppeditor.apply();
                            Log.d("WarningService", "Before Package : " + appChecker.getForegroundApp(getApplicationContext()));
                        }
                        else
                        {
                            // 네트워크 연결이 데이터 네트워크가 아닌 경우 알림 삭제
                            Util.cancelNotification(getApplicationContext(), Notification.PRIORITY_HIGH);

                            // 데이터 네트워크 아니면 이전 앱 기록을 지움
                            SharedPreferences beforeApps = getSharedPreferences("BeforeApp", Context.MODE_PRIVATE);
                            SharedPreferences.Editor beforeAppeditor = beforeApps.edit();
                            beforeAppeditor.putString("BeforeApp", "0");
                            beforeAppeditor.apply();
                        }
                    }
                }
            }, 0, 1, TimeUnit.SECONDS);
        }
        return START_STICKY;
    }


    // Foreground상에서 돌아가고 있는 앱을 확인하는 구문
    public boolean isForegroundApp(Context context, String fgapp)
    {
        AppChecker appChecker = new AppChecker();
        String packageName = appChecker.getForegroundApp(context);
        Log.d("WarningService", "Now Foreground Package Name : " + packageName);
        if(packageName.equals(fgapp)) {
            return true;
        }
        else
        {
            return false;
        }
    }
}
